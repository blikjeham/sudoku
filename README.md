# Sudoku solver

[![pipeline status](https://gitlab.com/blikjeham/sudoku/badges/main/pipeline.svg)](https://gitlab.com/blikjeham/sudoku/-/commits/main)

Sometimes when I try to solve a sudoku puzzle, I use pencil marks. I mark all fields with all possibilities, and then erase the pencil marks for the fields that are no longer possible.
Sometimes I get stuck.

The question I asked myself is, do I get stuck because I made a mistake, or do I get stuck because I'm not smart enough. So I wrote this program to see which of the two it was. The computer would not make mistakes, thus it would show me if I was lacking in my understanding of the puzzle.

It turns out that the program got stuck at exactly the same point. So it was my technique, not some sloppyness issue.

## Solving

How do I solve a sudoku? There are a lot of different techniques, but this is what I tought the program to do:

1. read the field from 'fields.sud'
2. set all possibilities where there is no value to ALL (0x1FF)
3. everywhere there is a value: remove the value from the possibilities in the same box, row, and column (we call that BRC).
4. if there is only one possibility left in a cell, set the value to that possibility.
5. goto step 3 until no more possibilities are removed
6. check for a single possibility within a BRC, this is where a number can only occur in one cell
7. goto step 3 again
8. check for twice the same possibility within a BRC. Remove the possibility from the other BRC's.
9. goto step 3 again
10. check for n times the same n possibilities within a BRC. For example twice a 1 and 2 in the same row means that neither 1 or 2 may occur in the same row apart from where they were found.
11. goto step 3 again
12. if no solution is found, give up.

## Bruteforcing

If no solutions has been found, we can present the user with the option to bruteforce the sudoku.

There are two options for bruteforcing.

### Manual bruteforcing

Here we ask the user to select which cell to brute force. This is done by first selecting B/R/C, then which BRC, then which cell within that BRC. Then we show the user all possibilities that are left, and then the user can enter the number.

From there, we will try to solve the puzzle again, until we are stuck again.

### Automatic bruteforcing

When automatic bruteforcing is selected, we first make a backup of the state of the puzzle. Then we seach for the cell with the fewest pencil marks, select one of the possibilities, and start solving the puzzle again. Until we get stuck again, and repeat the entire process.

If the puzzle becomes invalid, because we chose the wrong value, we restore the last backup and try again with a different value.
