#include <ncurses.h>

#include <strings.h>
#include <unistd.h>

#include "bf_util.h"
#include "ui.h"

/* initialize state */
int init(const char *filename)
{
	int res;

	if ((res = readfield(filename)))
		return res;

	if ((res = ui_init()))
		return res;

	fill_all();
	check_filled();
	printfield(TRUE);
	bruteforced = 0;

	return 0;
}

int main(int argc, char **argv)
{
	struct state state;
	char *filename;

	state_init(&state);

	if (argc > 1) {
		filename = argv[1];
	} else {
		filename = "fields.sud";
	}

	if (init(filename) < 0)
		return -1;

	prompt("Start solving");

	/* main loop */
	while (state.left > 0)
		solve(&state);

	if (final_check()) {
		message("Solved successfully");
	} else {
		message("There are still some errors");
	}
	printfield(TRUE);
	free_backups();
	press_any_key();
	endwin();
	return 0;
}
