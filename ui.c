#include "ui.h"
#include "s_util.h"

WINDOW *wfield;
WINDOW *wtext;

/* Initialize ncurses screen */
int ui_init(void)
{
	if (!initscr()) {
		printf("Error initializing ncurses\n");
		return -1;
	}
	/* Initialize the colors */
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLACK);
	init_pair(2, COLOR_BLACK, COLOR_WHITE);

	/* Set the windows */
	wfield = newwin(20, 46, 3, 3);
	wtext = newwin(18, 25, 3, 50);
	scrollok(wtext, TRUE);

	return 0;
}

void winprintf(WINDOW *wfield, const char *fmt, ...)
{
	char buffer[1024];
	va_list va;

	va_start(va, fmt);
	vsnprintf(buffer, 1023, fmt, va);
	waddstr(wfield, buffer);
	va_end(va);
}

static void addstring(WINDOW *wfield, const char *fmt, va_list va)
{
	char buffer[1024];

	vsnprintf(buffer, 1023, fmt, va);
	waddstr(wfield, buffer);
}

static void fieldprint(char *fmt, ...)
{
	va_list va;

	va_start(va, fmt);
	addstring(wfield, fmt, va);
	va_end(va);
}

/* add a newline to the text field */
void newline(void)
{
	waddstr(wtext, "\n\r");
	wrefresh(wtext);
}

/* write a message to the text field */
void message(const char *fmt, ...)
{
	va_list va;

	va_start(va, fmt);

	addstring(wtext, fmt, va);
	newline();
	va_end(va);
}

void message_n(const char *fmt, ...)
{
	va_list va;

	va_start(va, fmt);
	addstring(wtext, fmt, va);
	wrefresh(wtext);
	va_end(va);
}

static void print_border(int possible)
{
	if (possible) {
		fieldprint(" +-------------+-------------+-------------+");
	} else {
		fieldprint(" +-------+-------+-------+");
	}
}

static void print_value(struct cell *cell, int possible)
{
	if (cell->value) {
		if (possible) {
			if (cell->initial) {
				fieldprint(" %d  ", cell->value);
			} else {
				fieldprint(".%d. ", cell->value);
			}
		} else {
			fieldprint("%d ", cell->value);
		}
	} else {
		if (possible) {
			fieldprint("%03d ", cell->possible);

		} else {
			fieldprint(".  ");
		}
	}
}

static void print_footer(int possible)
{
	int i;

	if (possible) {
		for (i = 1; i < 10; i++) {
			fieldprint("%d=%d; ", i, vtom(i));
			if (i % 5 == 0)
				fieldprint("\n\r");
		}
	}
	fieldprint("\n\r");
	fieldprint("left: %d\n\r", get_left());
}

void printfield(int possible)
{
	int i;

	/* clear current field */
	wmove(wfield, 0, 0);
	werase(wfield);
	wrefresh(wfield);

	/* print header */
	if (possible) {
		fieldprint("    0   1   2     3   4   5     6   7   8\n\r");
	} else {
		fieldprint("   0 1 2   3 4 5   6 7 8\n\r");
	}
	print_border(possible);
	fieldprint("\n\r0| ");

	/* print rows */
	for (i = 0; i < 81; i++) {
		print_value(&puzzle->field[i], possible);
		if ((i % 81) != 80) {
			if ((i % 3) == 2)
				fieldprint("| ");
			if ((i % 27) == 26) {
				fieldprint("\n\r");
				print_border(possible);
			}
			if ((i % 9) == 8)
				fieldprint("\n\r%d| ", (i / 9) + 1);
		} else {
			fieldprint("|\n\r");
		}
	}
	print_border(possible);
	fieldprint("\n\r\n\r");
	print_footer(possible);
	wrefresh(wfield);
}

void printfield_bf(int brc, int where, int j)
{
	int i;
	wmove(wfield, 0, 0);

	winprintf(wfield, "    0   1   2     3   4   5     6   7   8\n\r");
	print_border(1);
	winprintf(wfield, "\n\r0| ");

	for (i = 0; i < 81; i++) {

		/* highlight the correct BRC */
		if (i_to_brc(brc, i) == where) {
			wcolor_set(wfield, 2, NULL);
		} else if (i == j) {
			/* highlight current selection */
			wcolor_set(wfield, 2, NULL);
		}
		print_value(&puzzle->field[i], 1);

		/* Reset the color */
		wcolor_set(wfield, 1, NULL);
		if ((i % 81) != 80) {
			if ((i % 3) == 2)
				winprintf(wfield, "| ");
			if ((i % 27) == 26) {
				winprintf(wfield, "\n\r ");
				print_border(1);
			}
			if ((i % 9) == 8)
				winprintf(wfield, "\n\r%d| ", (i / 9) + 1);
		} else {
			winprintf(wfield, "|\n\r");
		}
	}
	print_border(1);
	winprintf(wfield, "\n\r\n\r");
	print_footer(1);
	wrefresh(wfield);
}

char prompt(const char *fmt, ...)
{
	char value;
	va_list va;

	va_start(va, fmt);
	addstring(wtext, fmt, va);
	va_end(va);

	value = wgetch(wtext);
	newline();
	return value;
}

int prompt_i(const char *fmt, ...)
{
	va_list va;
	char value;

	va_start(va, fmt);
	addstring(wtext, fmt, va);
	va_end(va);

	value = wgetch(wtext);
	newline();
	if (value >= '0' && value <= '9')
		return value - '0';

	return -1;
}

int prompt_yesno(int def, char *fmt, ...)
{
	char yesno;
	va_list va;

	va_start(va, fmt);
	addstring(wtext, fmt, va);
	va_end(va);

	if (def) {
		message(" [Y/n]");
	} else {
		message(" [y/N]");
	}

	yesno = wgetch(wtext);
	newline();

	if (yesno == 'N' || yesno == 'n')
		return FALSE;

	if (yesno == 'Y' || yesno == 'y')
		return TRUE;

	return def;
}

void press_any_key(void)
{
	prompt("Press any key to continue...");
}
