
#include <ncurses.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "bf_util.h"
#include "ui.h"

int store_list(void)
{
	char *filename = malloc(80);
	FILE *f;
	int i;

	message("Enter the filename to store the sudoku in (empty to solve the "
		"puzzle): ");
	wscanw(wtext, "%80s", filename);

	if (strlen(filename) == 0)
		return 0;

	if (!(f = fopen(filename, "w"))) {
		winprintf(wtext, "Could not open file %s", filename);
		return -1;
	}
	for (i = 0; i < 81; i++) {
		fprintf(f, "%d", field[i].value);
	}
	fprintf(f, "\n");
	fclose(f);
	return 1;
}

void print_list(void)
{
	int i;
	for (i = 0; i < 81; i++)
		winprintf(wfield, "%d", field[i].value);
}

static void field_init(int i)
{
	field[i].value = 0;
	field[i].initial = 0;
	field[i].bftry = 0x0;
	field[i].possible = ALL;
}

static int initialize(void)
{
	if (ui_init())
		return -1;

	return 0;
}

int main(int argc, char **argv)
{
	struct state state;
	int i, ret, strict;

	state_init(&state);

	strict = 0;
	if (argc == 2) {
		if (!strcmp(argv[1], "--strict") || !strcmp(argv[1], "-s"))
			strict = 1;
	}

	if (initialize())
		return 1;

	for (i = 0; i < 81; i++) {
		field_init(i);
	}

	fill_all();
	state.left = get_left();

	for (i = 0; i < 81; i++) {
		printfield_bf(ROW, -1, i);
		ret = bf_enter_value(i);
		if (ret > 0) {
			field[i].initial = 1;
		} else if (ret == -1) {
			i--;
			field_init(i);
			fill_all();
			i--;
			continue;
		} else if (ret == -2) {
			break;
		}
		if (!strict) {
			solve(&state);
			state.count = 0;
		}
	}

	if (!store_list()) {
		state.count = 0;
		fill_all();
		state.left = get_left();
		while (state.left > 0) {
			if (!is_valid())
				invalid();

			solve(&state);

			if (!is_valid())
				invalid();

			state.left = get_left();
			state.count++;
		}
	}
	print_list();
	press_any_key();
	endwin();
	return 0;
}
