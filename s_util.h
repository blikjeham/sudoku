/* s_util.h
 *
 * sudoku utilities (i.e. helper functions)
 */

#ifndef S_UTIL_H
#define S_UTIL_H

#include <ncurses.h>
#include <stdio.h>

#include "const.h"

struct cell {
	unsigned int value : 8;
	unsigned int possible : 9;
	int index;
	int initial;
	int left;
	int brc[NUM_BRC];
	int bftry;
};

struct puzzle {
	struct puzzle *next;
	struct cell field[81];
	int depth;
};

struct single {
	int filled;
	int notfilled;
};

struct state {
	int left;          /* possibilities left */
	int previous_left; /* previous possibilities left */
	int count;         /* solve iteration */
};

extern struct puzzle *puzzle;

/* iterate over all cells */
#define foreach_cell(x) \
	for (int __i = 0; x = &puzzle->field[__i], __i < 81; __i++)

static inline void state_init(struct state *state)
{
	state->left = POSSIBLE_LEFT;
	state->previous_left = POSSIBLE_LEFT;
	state->count = 0;
}

/* Convert value to maks */
#define vtom(x) (1U << (x - 1))

/* Convert mask to value */
static inline int mtov(int mask)
{
	int i;

	for (i = 0; i < 10; i++) {
		if ((mask >> i) == 0)
			return i;
	}
	return -1;
}

/* Convert mask to number of possibilities */
static inline int mtop(int mask)
{
	int i;
	int count = 0;
	for (i = 1; i < 10; i++) {
		if (mask & vtom(i))
			count++;
	}
	return count;
}

/* get the cell mask */
static inline int cell_has_mask(struct cell *cell, int mask)
{
	if (cell->value)
		return 0;

	return (cell->possible & mask);
}

/* check strict mask of a cell */
static inline int cell_is_mask(struct cell *cell, int mask)
{
	if (cell->value)
		return 0;

	return (cell->possible == mask);
}

/* check if the sudoku is still valid.
 *
 * It is invalid if there is a field with no value and no possibilities left.
 */
int is_valid(void);

/* check the field for values that are filled.
 *
 * Remove the possibilities that are no longer possible.
 */
void check_filled(void);

/* check if a value is the only possibility in the field
 */
void check_only(void);

/* check for single value within a BRC */
void check_single(void);

/* check for double possibilities in BRC
 * This applies to one value possible within
 * a box on the same row. Delete it then from
 * all the other fields within the box, and on
 * the row. This goes for all the BRCs.
 */
void check_double(void);

/* check for double value possibilities in BRC
 * This means n values are possible in only n
 * fields of the BRC. All it does is look if the
 * mask occurs exactly the same number of times
 * as the number of values composing this mask.
 * The exact function checks if the possibilities
 * is the mask exactly. The loose
 * function allows for other possibilities in the
 * same field.
 * Hence: loose matching should be used with care.
 */
void check_double_value_exact(void);
void check_double_value_loose(void);

/* get the number of possible values left */
int get_left(void);

/* read the values from a file
 * Returns 0 on success
 */
int readfield(const char *);

/* fill all empty fields with all possibilities
 * We will remove these possibilities one by one
 * later on.
 * check_filled should be invoked as soon as
 * possible after fill_all.
 */
void fill_all(void);

static inline int brc_to_i(int brc, int which, int where)
{
	if (brc == BOX) {
		return ((((which / 3) * 3) * 9) + ((which % 3) * 3))
			+ (((where / 3) * 9) + where % 3);
	}
	if (brc == ROW) {
		return (which * 9) + where;
	}
	if (brc == COL) {
		return (where * 9) + which;
	}
	return -1;
}

static inline int i_to_brc(int brc, int where)
{
	switch (brc) {
	case BOX:
		return ((i_to_brc(ROW, where) / 3) * 3)
			+ (i_to_brc(COL, where) / 3);
	case ROW:
		return where / 9;
	case COL:
		return where % 9;
	}
	return 0;
}

/* return TRUE if p sees q */
static inline int is_seen(int p, int q)
{
	int brc;

	for (brc = BOX; brc < NUM_BRC; brc++) {
		if (i_to_brc(brc, p) == i_to_brc(brc, q))
			return TRUE;
	}
	return FALSE;
}

static inline int brc_to_mask(int brc, int i)
{
	return vtom(i_to_brc(brc, i) + 1);
}

/* these functions will convert a number
 * between 0 and 81 to a box, row, or column ranging
 * from 0 to 9
 */

void set_value(struct cell *, int);
void set_num(int, int, int);

/* convert a possibilities field to a single value */
int get_value(int);

/* Final validity check, just to be sure */
int final_check(void);

int invalid(void);

/* Main solve routine */
void solve(struct state *);

#endif /* S_UTIL_H */
