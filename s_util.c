#include <stdlib.h>
#include <strings.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "bf_util.h"
#include "ui.h"

struct puzzle *puzzle;

static void remove_possible(struct cell *cell, int mask)
{
	int removed;

	if (cell->value > 0 || !(cell->possible & mask))
		return;

	removed = cell->possible & mask;
	cell->possible &= ~mask;
	cell->left -= mtop(removed);
}

static void fill_brc_ex(int brc, int where, int *excl, int mask)
{
	struct cell *cell;
	int rcb, cbr;

	rcb = (brc + 1) % 3;
	cbr = (brc + 2) % 3;

	foreach_cell(cell) {
		if (cell->brc[brc] == where)
			continue;

		if ((excl[rcb] == brc_to_mask(rcb, cell->index)
			    || excl[cbr] == brc_to_mask(cbr, cell->index))
			&& cell_has_mask(cell, mask)) {

			message("fill_brc_ex");
			remove_possible(cell, mask);
		}
	}
}

static void fill_brc_double(int brc, int where, int *excl, int mask)
{
	struct cell *cell;
	int rcb, cbr;

	foreach_cell(cell) {
		if (cell->brc[brc] != where)
			continue;

		rcb = (brc + 1) % 3;
		cbr = (brc + 2) % 3;
		if ((!(excl[rcb] & brc_to_mask(rcb, cell->index))
			    || (!(excl[cbr] & brc_to_mask(cbr, cell->index))))
			&& cell_has_mask(cell, mask)) {

			message("fill_brc_double");
			remove_possible(cell, mask);
		}
	}
}

/* Remove the possibilities from a value from a BRC */
static void fill_brc(int where, int value)
{
	struct cell *cell;

	/* Remove the possibility of value for each field in the box, row, or
	 * column */
	foreach_cell(cell) {
		if (is_seen(where, cell->index)) {
			remove_possible(cell, vtom(value));
		}
	}
}

/* Check for the occurance of a mask in the brc
 * 'normal' (i.e. loose) function allows for other
 * possibilities to be present.
 * exact only matches the mask when no other possibilities
 * are present.
 */
static int check_num_brc(int brc, int *excl, int where, int mask)
{
	struct cell *cell;
	int count = 0;

	foreach_cell(cell) {
		if (cell->brc[brc] != where)
			continue;

		if (cell_has_mask(cell, mask) == mask) {
			count++;
			excl[BOX] |= brc_to_mask(BOX, cell->index);
			excl[ROW] |= brc_to_mask(ROW, cell->index);
			excl[COL] |= brc_to_mask(COL, cell->index);
		}
	}
	return count;
}

static int check_num_brc_exact(int brc, int *excl, int where, int mask)
{
	struct cell *cell;
	int count;
	int rcb, cbr;

	rcb = (brc + 1) % 3;
	cbr = (brc + 2) % 3;
	count = 0;

	foreach_cell(cell) {
		if (cell->brc[brc] != where)
			continue;

		if (cell_is_mask(cell, mask)) {
			count++;
			excl[rcb] |= brc_to_mask(rcb, cell->index);
			excl[cbr] |= brc_to_mask(cbr, cell->index);
		}
	}
	return count;
}

static int check_num_brc_loose(int brc, int *excl, int where, int mask)
{
	struct cell *cell;
	int count = 0;
	int extracount = 0;

	foreach_cell(cell) {
		if (cell->brc[brc] != where)
			continue;

		if (cell_has_mask(cell, mask) == mask) {

			count++;
			excl[BOX] |= brc_to_mask(BOX, cell->index);
			excl[ROW] |= brc_to_mask(ROW, cell->index);
			excl[COL] |= brc_to_mask(COL, cell->index);
		} else if (cell_has_mask(cell, mask)) {
			extracount++;
		}
	}
	return (extracount * 10) + count;
}

int check_num(int brc, int where, int mask)
{
	struct cell *cell;
	int num = 0;

	foreach_cell(cell) {
		if (cell->brc[brc] != where)
			continue;

		if (cell_has_mask(cell, mask))
			num++;
	}
	return num;
}

int is_valid(void)
{
	struct cell *cell;

	foreach_cell(cell) {
		if ((cell->value == 0) && (cell->possible == 0))
			return FALSE;
	}
	return TRUE;
}

void check_filled(void)
{
	struct cell *cell;

	foreach_cell(cell) {
		if (cell->value > 0)
			fill_brc(cell->index, cell->value);
	}
}

void check_only(void)
{
	struct cell *cell;
	int value;

	foreach_cell(cell) {
		if ((cell->value == 0) && (cell->left == 1)) {
			value = mtov(cell->possible);
			set_value(cell, value);
			fill_brc(cell->index, value);
		}
	}
}

void check_single(void)
{
	int i, num;
	int brc;

	for (num = 1; num < 10; num++) {
		for (i = 0; i < 9; i++) {
			for (brc = BOX; brc < NUM_BRC; brc++) {
				if (check_num(brc, i, vtom(num)) == 1) {
					set_num(brc, i, num);
				}
			}
		}
	}
}

void check_double(void)
{
	int num;
	int i;
	int a_brc[3] = {0, 0, 0};

	for (num = 1; num < 10; num++) {
		for (i = 0; i < 9; i++) {
			bzero(a_brc, sizeof(int) * 3);
			if (check_num_brc(BOX, a_brc, i, vtom(num)) == 2)
				fill_brc_ex(BOX, i, a_brc, vtom(num));

			if (check_num_brc(ROW, a_brc, i, vtom(num)) == 2)
				fill_brc_ex(ROW, i, a_brc, vtom(num));

			if (check_num_brc(COL, a_brc, i, vtom(num)) == 2)
				fill_brc_ex(COL, i, a_brc, vtom(num));
		}
	}
}

void check_double_value_exact(void)
{
	int i;
	int mask;
	int a_brc[3] = {0, 0, 0};
	int brc = 0;

	for (mask = ONE; mask <= ALL; mask++) {
		for (i = 0; i < 9; i++) {
			for (brc = BOX; brc < NUM_BRC; brc++) {
				bzero(a_brc, sizeof(int) * 3);
				if (check_num_brc_exact(brc, a_brc, i, mask)
					== mtop(mask)) {

					fill_brc_double(brc, i, a_brc, mask);
					printfield(TRUE);
					check_single();
				}
			}
		}
	}
}

void check_double_value_loose(void)
{
	int i;
	int mask;
	int a_brc[3] = {0, 0, 0};
	int brc = 0;
	int ret = 0;

	for (mask = ONE; mask <= ALL; mask++) {
		for (i = 0; i < 9; i++) {
			for (brc = BOX; brc < NUM_BRC; brc++) {
				bzero(a_brc, sizeof(int) * 3);
				ret = check_num_brc_loose(brc, a_brc, i, mask);
				if ((ret < 10) && (ret == mtop(mask))) {
					fill_brc_double(brc, i, a_brc, mask);
					check_single();
				}
			}
		}
	}
}

int get_left(void)
{
	struct cell *cell;
	int left = 0;

	foreach_cell(cell) {
		left += cell->left;
	}
	return left;
}

int readfield(const char *filename)
{
	struct cell *cell;
	FILE *fd;
	int i;
	char number;

	if (!(fd = fopen(filename, "r"))) {
		printf("error opening file\n");
		return -1;
	}

	if (puzzle)
		return -1; /* already initialized */

	puzzle = malloc(sizeof(struct puzzle));
	puzzle->next = NULL;
	puzzle->depth = 0;

	for (i = 0; i < 81; i++) {
		number = fgetc(fd);
		if ((number < 0x30) || (number > 0x3a)) {
			/* the character read is actually not a number
			   so skip it and continue. */
			i--;
			continue;
		}
		cell = &puzzle->field[i];
		cell->value = (number & ~0x30);
		/* Set the initial flag
		   This will be used in printing the field*/
		if (cell->value > 0) {
			cell->initial = 1;
		} else {
			cell->initial = 0;
		}
		cell->bftry = 0x0;
	}
	return 0;
}

void fill_all(void)
{
	int i;

	for (i = 0; i < 81; i++) {
		puzzle->field[i].index = i;
		puzzle->field[i].brc[BOX] = i_to_brc(BOX, i);
		puzzle->field[i].brc[ROW] = i_to_brc(ROW, i);
		puzzle->field[i].brc[COL] = i_to_brc(COL, i);
		if (puzzle->field[i].value == 0) {
			puzzle->field[i].possible = ALL;
			puzzle->field[i].left = 9;
		}
	}
}

void set_value(struct cell *cell, int value)
{
	cell->value = value;
	cell->possible = 0;
	cell->left = 0;
}

void set_num(int brc, int where, int value)
{
	struct cell *cell;
	int mask;

	mask = vtom(value);

	foreach_cell(cell) {
		if (cell->brc[brc] != where)
			continue;

		if (cell_has_mask(cell, mask)) {

			/* set the value */
			set_value(cell, value);
			fill_brc(cell->index, value);
		}
	}
}

/* is the number still a possibility? */
int get_value(int possib)
{
	if (possib & ONE)
		return 1;
	if (possib & TWO)
		return 2;
	if (possib & THREE)
		return 3;
	if (possib & FOUR)
		return 4;
	if (possib & FIVE)
		return 5;
	if (possib & SIX)
		return 6;
	if (possib & SEVEN)
		return 7;
	if (possib & EIGHT)
		return 8;
	if (possib & NINE)
		return 9;
	return 0;
}

static void prompt_autobf()
{
	char fb;

	fb = prompt("Bruteforce first of best empty field? [f/B]");
	if (fb == 'f' || fb == 'F') {
		bruteforced = BF_FIRST;
	} else {
		bruteforced = BF_BEST;
	}
}

/* start bruteforcing when stuck */
static void stuck(struct state *state)
{
	char yesno;
	int i;

	if (bruteforced > BF_MANUAL) {
		autobruteforce();
		state_init(state);
		fill_all();
		check_filled();
		printfield(TRUE);
	} else {
		message("Unsolvable?");

		printfield(TRUE);

		yesno = prompt("Do you wish to bruteforce the sudoku? [a/y/N]");
		if (yesno == 'Y' || yesno == 'y') {

			bruteforced = BF_MANUAL;

			/* bruteforce */
			bruteforce();

			/* reset the values since they are no longer to be */
			/* trusted. */
			state_init(state);
			fill_all();
			check_filled();
			printfield(TRUE);

		} else if (yesno == 'A' || yesno == 'a') {

			prompt_autobf();

			/* Automatically brute force the sudoku */
			autobruteforce();
			state_init(state);
			fill_all();
			check_filled();
			printfield(TRUE);
		} else {

			/* Print the remaining array, so we can save it */
			for (i = 0; i < 81; i++)
				winprintf(wfield, "%d", puzzle->field[i].value);

			winprintf(wfield, "\n\r");
			wrefresh(wfield);
			endwin();
			exit(1);
		}
	}
}

int final_check(void)
{
	struct cell *cell;
	int where;
	int brc;
	int mask;

	message("Final check.");
	for (where = 0; where < 9; where++) {
		for (brc = BOX; brc < NUM_BRC; brc++) {
			mask = 0x0;

			/* print the field using highlights. */
			printfield_bf(brc, where, -1);
			foreach_cell(cell) {
				if (cell->brc[brc] != where)
					mask |= vtom(cell->value);
			}
			if (mask != ALL)
				return FALSE;
		}
	}
	return TRUE;
}

int invalid(void)
{
	message("Sudoku is invalid.");
	if (bruteforced == 1) {
		if (prompt_yesno(TRUE, "Do you wish to restore the backup?")) {
			message("Restoring backup. You should try again.");
			if (restore_backup() == -1)
				message("Restore not possible");

			bruteforced = 0;
		}
	}
	if (bruteforced > 1) {
		if (restore_backup() == -1)
			message("Restore not possible");
	}
	return 0;
}

void solve(struct state *state)
{
	if (!is_valid())
		invalid();

	printfield(TRUE);
	check_filled();
	printfield(TRUE);

	check_only();
	printfield(TRUE);

	state->left = get_left();

	if (state->left != state->previous_left) {
		state->previous_left = state->left;
		state->count = 0;
	} else {
		switch (state->count) {
		case 0:
			check_single();
			break;
		case 1:
			check_double();
			break;
		case 2:
			check_double_value_exact();
			break;
		case 3:
			check_double_value_loose();
			break;
		default:
			if (state->left > 0)
				stuck(state);

			break;
		}
	}
	state->count += 1;
}
