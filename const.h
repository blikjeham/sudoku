/* const.h
 *
 * type declarations etc for sudoku
 */

#ifndef CONST_H
#define CONST_H

#define ONE   0x001
#define TWO   0x002
#define THREE 0x004
#define FOUR  0x008
#define FIVE  0x010
#define SIX   0x020
#define SEVEN 0x040
#define EIGHT 0x080
#define NINE  0x100
#define ALL   0x1ff

enum BRC {
	BOX = 0,
	ROW,
	COL,
	NUM_BRC
};

/* types of brute forcing */
enum BF {
	BF_MANUAL = 1,
	BF_FIRST,
	BF_BEST,
};

#define POSSIBLE_LEFT (9 * 9 * 9)

#define FALSE 0
#define TRUE 1

#endif /* CONST_H */
