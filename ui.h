#ifndef UI_H
#define UI_H

#include <ncurses.h>

#include "const.h"

extern WINDOW *wfield;
extern WINDOW *wtext;

/* Initialize ncurses screen */
int ui_init(void);

/* printf to a window if we have ncurses
 * else we will print it to stdout.
 */
void winprintf(WINDOW *, const char *, ...);

/* add newline to the text window */
void newline();

/* add message to the text window */
void message(const char *, ...);

/* add message without newline to text window */
void message_n(const char *, ...);

/* print the field so far
 * If the argument is true, print the
 * possibilities alongside the actual values.
 * That should make debugging easier.
 */
void printfield(int);

/* print the field with bruteforce highlighting */
void printfield_bf(int, int, int);

/* prompt for a single character */
char prompt(const char *, ...);

/* prompt for a number */
int prompt_i(const char *, ...);

/* prompt for y/n */
int prompt_yesno(int, char *, ...);

/* prompt for any key */
void press_any_key(void);

#endif /* UI_H */
