#ifndef BF_UTIL_H
#define BF_UTIL_H

#include "s_util.h"

extern int bruteforced;

int bf_enter_value(int);

void bruteforce(void);
void autobruteforce(void);

int restore_backup(void);

void free_backups(void);

#endif /* BF_UTIL_H */
