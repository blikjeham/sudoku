#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "bf_util.h"
#include "ui.h"

int bruteforced;

static int is_bf_able(int where)
{
	if (puzzle->field[where].initial > 0) {
		message("Initial value: %d", puzzle->field[where].initial);
		return FALSE;
	}
	if (puzzle->field[where].value > 0) {
		message("Value: %d", puzzle->field[where].value);
		return FALSE;
	}
	if (puzzle->field[where].bftry == puzzle->field[where].possible) {
		message("Already tried those possibilities");
		return FALSE;
	}
	return TRUE;
}

static int is_abf_able(int where)
{
	if ((puzzle->field[where].initial > 0)
		|| (puzzle->field[where].value > 0)
		|| puzzle->field[where].bftry
			== puzzle->field[where].possible) {
		return FALSE;
	} else {
		return TRUE;
	}
}

static void bf_set_value(int i, int value)
{
	set_value(&puzzle->field[i], value);

	/* set bruteforce values */
	puzzle->field[i].bftry |= vtom(value);

	if (puzzle->next)
		puzzle->next->field[i].bftry |= vtom(value);
}

int bf_enter_value(int i)
{
	int num;
	int value = -1;

	message_n("Possible values are: ");
	for (num = 1; num <= 9; num++) {
		if (puzzle->field[i].possible & vtom(num))
			message_n("%d ", num);
	}
	newline();
	message_n("Already tried: ");
	for (num = 1; num <= 9; num++) {
		if (puzzle->field[i].bftry & vtom(num))
			message_n("%d ", num);
	}
	newline();
	while (value < 0 || value > 9) {
		message_n("Enter a value: ");
		value = wgetch(wtext);
		newline();
		if (value == KEY_BACKSPACE || value == '-') {
			return -1;
		} else if (value == 'q') {
			return -2;
		}
		value = value - '0';
	}
	if ((puzzle->field[i].possible & vtom(value))) {
		bf_set_value(i, value);
	} else if (value == 0) {
		puzzle->field[i].initial = 0;
		return 0;
	} else {
		return bf_enter_value(i);
	}
	return 1;
}

static void bf_getfield(int brc, int where)
{
	int which = -1;
	int i = -1;

	wclear(wfield);
	wrefresh(wfield);

	printfield_bf(brc, where, -1);
	while (which < 0 || which > 8)
		which = prompt_i("Please select a field:");

	i = brc_to_i(brc, where, which);
	if (is_bf_able(i)) {
		bf_enter_value(i);
	} else {
		sleep(3);
		bruteforce();
	}
}

static void bf_box(void)
{
	int box = -1;
	while (box < 0 || box > 8)
		box = prompt_i("Please select a box:");

	message("Selected box %d", box);
	bf_getfield(BOX, box);
}

static void bf_row(void)
{
	int row = -1;
	while (row < 0 || row > 8)
		row = prompt_i("Please select a row:");

	message("Selected row %d", row);
	bf_getfield(ROW, row);
}

static void bf_col(void)
{
	int col = -1;
	while (col < 0 || col > 8)
		col = prompt_i("Please select a column:");

	message("Selected column %d", col);
	bf_getfield(COL, col);
}

static int make_backup(void)
{
	struct puzzle *new;
	int i;

	message("Backing up the field for undo.");

	new = malloc(sizeof(struct puzzle));

	/* copy fields current fields */
	for (i = 0; i < 81; i++)
		new->field[i] = puzzle->field[i];

	new->depth = puzzle->depth + 1;
	new->next = puzzle;
	puzzle = new;
	message("M-Backup depth: %d", new->depth);
	return 0;
}

int restore_backup(void)
{
	struct puzzle *backup;

	/* this is the last state */
	if (!(backup = puzzle->next))
		return -1;

	free(puzzle);
	puzzle = backup;
	message("R-Backup depth: %d", backup->depth);
	return 0;
}

void bruteforce(void)
{
	char brc = 0;

	wclear(wtext);
	wrefresh(wtext);
	make_backup();

	brc = prompt("Select a box, row, or\n\rcolumn [b/r/c]?");
	switch (brc) {
	case 'b':
	case 'B':
		bf_box();
		break;
	case 'r':
	case 'R':
		bf_row();
		break;
	case 'c':
	case 'C':
		bf_col();
		break;
	default:
		message("Invalid choice.");
		bruteforce();
		break;
	}
}

static int find_first_abfable(void)
{
	int i = 0;
	for (i = 0; i < 81; i++) {
		if (is_abf_able(i)) {
			return i;
		}
	}
	return -1;
}

static int find_best_abfable(void)
{
	int i = 0;
	int bestfield = -1;
	int mincount = 0;

	for (i = 0; i < 81; i++) {
		if ((is_abf_able(i)) && (puzzle->field[i].left > mincount)) {
			bestfield = i;
			mincount = puzzle->field[i].left;
		}
	}
	return bestfield;
}

void autobruteforce(void)
{
	int (*find)(void);
	int i = 0;
	int num = 0;

	if (bruteforced == BF_FIRST) {
		find = find_first_abfable;
	} else {
		find = find_best_abfable;
	}

	/* find the first field that is bruteforcable */
	if ((i = find()) == -1) {
		message("No more brutefore possibilities.");
		message("Restoring backup");
		restore_backup();
		return;
	}

	make_backup();
	message("Tries: %d", bruteforced);
	for (num = 1; num < 10; num++) {

		if ((puzzle->field[i].possible & vtom(num))
			&& !(puzzle->field[i].bftry & vtom(num))) {

			message("Bruteforcing %d(%dx%d)\n\rvalue %d", i,
				i_to_brc(COL, i), i_to_brc(ROW, i), num);

			bf_set_value(i, num);
		}
	}
}

void free_backups(void)
{
	struct puzzle *p;

	while ((p = puzzle)) {
		message("freeing backup %d", p->depth);
		puzzle = p->next;
		free(p);
	}
}
